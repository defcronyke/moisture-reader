EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 9843 6299
encoding utf-8
Sheet 1 1
Title "Moisture Reader"
Date "2020-11-09"
Rev "0.1.2"
Comp "https://eternalvoid.net"
Comment1 "Battery Case with Switch: https://www.aliexpress.com/item/32832684174.html"
Comment2 "Battery Protection Board: https://www.aliexpress.com/item/4000141681288.html"
Comment3 "LiFePo4 Battery: https://www.aliexpress.com/item/4001141997998.html"
Comment4 "Project Page: https://gitlab.com/defcronyke/moisture-reader"
$EndDescr
$Comp
L 74xx:74HC595 U1
U 1 1 5F9D6DC5
P 2200 2000
F 0 "U1" V 2246 1256 50  0000 R CNN
F 1 "74HC595" V 2155 1256 50  0000 R CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 2200 2000 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74hc595.pdf" H 2200 2000 50  0001 C CNN
	1    2200 2000
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74HC595 U2
U 1 1 5F9D9A0C
P 3950 2000
F 0 "U2" V 3996 1256 50  0000 R CNN
F 1 "74HC595" V 3905 1256 50  0000 R CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 3950 2000 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74hc595.pdf" H 3950 2000 50  0001 C CNN
	1    3950 2000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2900 2000 2900 2800
Wire Wire Line
	4650 2000 4650 2800
Wire Wire Line
	4650 2800 4150 2800
Connection ~ 2900 2800
Wire Wire Line
	2400 2400 2400 2800
Wire Wire Line
	2100 2400 2100 2600
$Comp
L Display_Character:HDSP-A153 U4
U 1 1 5F9E86D3
P 6800 2000
F 0 "U4" H 6800 2667 50  0000 C CNN
F 1 "HDSP-A153" H 6800 2576 50  0000 C CNN
F 2 "Display_7Segment:KCSC02-105" H 6800 1450 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-2553EN" H 6400 2550 50  0001 C CNN
	1    6800 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 2000 3350 1050
Wire Wire Line
	5300 2400 5300 2800
Wire Wire Line
	5300 2800 4650 2800
Connection ~ 4650 2800
Wire Wire Line
	6500 2400 6500 2800
Wire Wire Line
	6500 2800 5900 2800
Connection ~ 5300 2800
$Comp
L Device:R R1
U 1 1 5F9FC336
P 5900 2600
F 0 "R1" H 5970 2646 50  0000 L CNN
F 1 "470" H 5970 2555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5830 2600 50  0001 C CNN
F 3 "~" H 5900 2600 50  0001 C CNN
	1    5900 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5F9FCCE8
P 7100 2600
F 0 "R2" H 7170 2646 50  0000 L CNN
F 1 "470" H 7170 2555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7030 2600 50  0001 C CNN
F 3 "~" H 7100 2600 50  0001 C CNN
	1    7100 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 2450 5900 2400
Wire Wire Line
	5900 2750 5900 2800
Connection ~ 5900 2800
Wire Wire Line
	5900 2800 5300 2800
Wire Wire Line
	7100 2450 7100 2400
Wire Wire Line
	7100 2750 7100 2800
Wire Wire Line
	7100 2800 6500 2800
Connection ~ 6500 2800
Wire Wire Line
	4150 2400 4150 2800
Connection ~ 4150 2800
Wire Wire Line
	4150 2800 2900 2800
Wire Wire Line
	3850 2400 3850 2550
Wire Wire Line
	3850 2550 3350 2550
Wire Wire Line
	3350 2550 3350 2000
Connection ~ 3350 2000
Wire Wire Line
	2400 1600 2400 1400
Wire Wire Line
	2400 1400 6500 1400
Wire Wire Line
	6500 1400 6500 1700
Wire Wire Line
	2300 1600 2300 1350
Wire Wire Line
	2300 1350 6450 1350
Wire Wire Line
	6450 1350 6450 1800
Wire Wire Line
	6450 1800 6500 1800
Wire Wire Line
	2200 1600 2200 1300
Wire Wire Line
	6400 1300 6400 1900
Wire Wire Line
	6400 1900 6500 1900
Wire Wire Line
	2200 1300 6400 1300
Wire Wire Line
	2100 1600 2100 1250
Wire Wire Line
	2100 1250 6350 1250
Wire Wire Line
	6350 1250 6350 2000
Wire Wire Line
	6350 2000 6500 2000
Wire Wire Line
	2000 1600 2000 1200
Wire Wire Line
	2000 1200 6300 1200
Wire Wire Line
	6300 1200 6300 2100
Wire Wire Line
	6300 2100 6500 2100
Wire Wire Line
	1900 1600 1900 1150
Wire Wire Line
	1900 1150 6250 1150
Wire Wire Line
	6250 1150 6250 2200
Wire Wire Line
	6250 2200 6500 2200
Wire Wire Line
	1800 1600 1800 1100
Wire Wire Line
	1800 1100 6200 1100
Wire Wire Line
	6200 1100 6200 2300
Wire Wire Line
	6200 2300 6500 2300
Wire Wire Line
	4150 1600 4150 1000
Wire Wire Line
	4150 1000 5300 1000
Wire Wire Line
	5300 1000 5300 1700
Wire Wire Line
	4050 1600 4050 950 
Wire Wire Line
	4050 950  5250 950 
Wire Wire Line
	5250 950  5250 1800
Wire Wire Line
	5250 1800 5300 1800
Wire Wire Line
	3950 1600 3950 900 
Wire Wire Line
	3950 900  5200 900 
Wire Wire Line
	5200 900  5200 1900
Wire Wire Line
	5200 1900 5300 1900
Wire Wire Line
	3850 1600 3850 850 
Wire Wire Line
	3850 850  5150 850 
Wire Wire Line
	5150 850  5150 2000
Wire Wire Line
	5150 2000 5300 2000
Wire Wire Line
	3750 1600 3750 800 
Wire Wire Line
	3750 800  5100 800 
Wire Wire Line
	5100 800  5100 2100
Wire Wire Line
	5100 2100 5300 2100
Wire Wire Line
	3650 1600 3650 750 
Wire Wire Line
	3650 750  5050 750 
Wire Wire Line
	5050 750  5050 2200
Wire Wire Line
	5050 2200 5300 2200
Wire Wire Line
	3550 1600 3550 700 
Wire Wire Line
	3550 700  5000 700 
Wire Wire Line
	5000 700  5000 2300
Wire Wire Line
	5000 2300 5300 2300
$Comp
L Device:R R4
U 1 1 5FA33CD9
P 2900 3150
F 0 "R4" H 2970 3196 50  0000 L CNN
F 1 "10k" H 2970 3105 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2830 3150 50  0001 C CNN
F 3 "~" H 2900 3150 50  0001 C CNN
	1    2900 3150
	1    0    0    -1  
$EndComp
Connection ~ 2400 2800
Wire Wire Line
	2900 3000 2900 2800
Wire Wire Line
	1300 3150 1300 3400
Wire Wire Line
	2000 3150 2000 3450
Wire Wire Line
	2000 3450 2900 3450
Wire Wire Line
	2900 3450 2900 3300
Wire Wire Line
	2700 1600 3300 1600
Wire Wire Line
	3300 1600 3300 2400
Wire Wire Line
	3300 2400 3550 2400
Wire Wire Line
	2000 2400 2000 2500
Wire Wire Line
	2000 2500 3750 2500
Wire Wire Line
	3750 2500 3750 2400
Wire Wire Line
	2300 2400 2300 2700
Wire Wire Line
	2300 2700 4050 2700
Wire Wire Line
	4050 2700 4050 2400
$Comp
L Connector:Conn_01x09_Male J1
U 1 1 5FA4F02A
P 3250 3850
F 0 "J1" H 3358 4431 50  0000 C CNN
F 1 "Conn_01x09_Male" H 3358 4340 50  0000 C BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x09_P2.54mm_Horizontal" H 3250 3850 50  0001 C CNN
F 3 "~" H 3250 3850 50  0001 C CNN
	1    3250 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 2400 1500 2400
Wire Wire Line
	1500 2400 1500 1500
Wire Wire Line
	1500 1500 1000 1500
Wire Wire Line
	2000 2500 1450 2500
Wire Wire Line
	1450 2500 1450 1650
Wire Wire Line
	1450 1650 1000 1650
Connection ~ 2000 2500
Wire Wire Line
	900  3150 900  2600
Connection ~ 1600 2000
Connection ~ 1600 2600
Wire Wire Line
	1600 2600 1600 2000
Wire Wire Line
	2300 2700 1400 2700
Wire Wire Line
	1400 2700 1400 1800
Wire Wire Line
	1400 1800 1000 1800
Connection ~ 2300 2700
Wire Wire Line
	1600 3150 1600 2600
Wire Wire Line
	1600 1050 3350 1050
Text GLabel 3450 3850 2    50   Input ~ 0
SER
Text GLabel 3450 3950 2    50   Input ~ 0
SRCLK
Text GLabel 1000 1650 0    50   Input ~ 0
SRCLK
Text GLabel 3450 4050 2    50   Input ~ 0
RCLK
Text GLabel 1000 1500 0    50   Input ~ 0
SER
Text GLabel 1000 1800 0    50   Input ~ 0
RCLK
Text GLabel 1450 3400 3    50   Output ~ 0
CALMIN
Text GLabel 3450 4250 2    50   Output ~ 0
CALMAX
Text GLabel 2200 3450 3    50   Output ~ 0
CALMAX
Text GLabel 3450 4150 2    50   Output ~ 0
CALMIN
NoConn ~ 5900 2300
NoConn ~ 7100 2300
NoConn ~ 4450 1600
NoConn ~ 4250 1600
NoConn ~ 2500 1600
Text GLabel 1000 1050 0    50   Input ~ 0
VIN
Text GLabel 1000 1200 0    50   Input ~ 0
GND
Wire Wire Line
	1000 1200 1150 1200
Text GLabel 3700 2950 2    50   Input ~ 0
VIN
Text GLabel 3700 3100 2    50   Input ~ 0
GND
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5FBF5BB0
P 3700 2950
F 0 "#FLG0101" H 3700 3025 50  0001 C CNN
F 1 "PWR_FLAG" V 3700 3077 50  0000 L CNN
F 2 "" H 3700 2950 50  0001 C CNN
F 3 "~" H 3700 2950 50  0001 C CNN
	1    3700 2950
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR0102
U 1 1 5FC046E4
P 1150 1050
F 0 "#PWR0102" H 1150 900 50  0001 C CNN
F 1 "+3.3V" H 1165 1178 50  0000 L TNN
F 2 "" H 1150 1050 50  0001 C CNN
F 3 "" H 1150 1050 50  0001 C CNN
	1    1150 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 1050 1150 1050
Connection ~ 1150 1050
Wire Wire Line
	1150 1050 1600 1050
Text GLabel 3450 3650 2    50   Input ~ 0
VIN
Text GLabel 3450 3750 2    50   Input ~ 0
GND
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5FC25CE2
P 3700 3100
F 0 "#FLG0102" H 3700 3175 50  0001 C CNN
F 1 "PWR_FLAG" V 3700 3228 50  0000 L CNN
F 2 "" H 3700 3100 50  0001 C CNN
F 3 "~" H 3700 3100 50  0001 C CNN
	1    3700 3100
	0    -1   -1   0   
$EndComp
Text GLabel 3450 3450 2    50   Output ~ 0
VOUT
Text GLabel 1000 750  0    50   Output ~ 0
VOUT
Connection ~ 1600 1050
Text GLabel 1000 900  0    50   Output ~ 0
GND
Wire Wire Line
	1600 750  1000 750 
Wire Wire Line
	1600 750  1600 1050
Wire Wire Line
	1000 900  1550 900 
Wire Wire Line
	1550 900  1550 1200
Wire Wire Line
	1600 1050 1600 2000
Connection ~ 1550 1200
Wire Wire Line
	1550 1200 1550 2800
Text GLabel 3450 3550 2    50   Output ~ 0
GND
Text Notes 3850 4400 0    63   ~ 0
Pins 1 & 2 (VOUT & GND) - 3.3V DC to power the ESP32 microcontroller (ESP32 pins 3.3V & GND)\nPins 3 & 4 (VIN & GND) - 3.3V DC power from a LiFePo4 battery (requires an external protection circuit)\nPin 5 (SER) - digit display data into the shift registers (ESP32 GPIO pin 32)\nPin 6 (SRCLK) - set the shift registers into the data accepting state (ESP32 GPIO pin 25)\nPin 7 (RCLK) - output the contents of the shift registers (ESP32 GPIO pin 33)\nPin 8 (CALMIN) - signal the ESP32 to begin dry calibration (ESP32 GPIO pin 26)\nPin 9 (CALMAX) - signal the ESP32 to begin wet calibration (ESP32 GPIO pin 27)\n\nThe moisture sensor needs a 10k / 15k ohm voltage divider on its analog output line, \nand plugs into the ESP32, not this board (ESP32 GPIO pin 34).
Wire Wire Line
	1300 3400 2400 3400
Wire Wire Line
	2400 2800 1550 2800
Wire Wire Line
	2400 3400 2400 3300
Wire Wire Line
	2400 3000 2400 2800
$Comp
L Device:R R3
U 1 1 5FA29A07
P 2400 3150
F 0 "R3" H 2470 3196 50  0000 L CNN
F 1 "10k" H 2470 3105 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2330 3150 50  0001 C CNN
F 3 "~" H 2400 3150 50  0001 C CNN
	1    2400 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 2800 2900 2800
$Comp
L power:GND #PWR0101
U 1 1 5FA7FCA9
P 1150 1200
F 0 "#PWR0101" H 1150 950 50  0001 C CNN
F 1 "GND" H 1155 1027 50  0000 L BNN
F 2 "" H 1150 1200 50  0001 C CNN
F 3 "" H 1150 1200 50  0001 C CNN
	1    1150 1200
	1    0    0    -1  
$EndComp
Connection ~ 1150 1200
Wire Wire Line
	1150 1200 1550 1200
$Comp
L Display_Character:HDSP-A153 U3
U 1 1 5F9E7EB5
P 5600 2000
F 0 "U3" H 5600 2667 50  0000 C CNN
F 1 "HDSP-A153" H 5600 2576 50  0000 C CNN
F 2 "Display_7Segment:KCSC02-105" H 5600 1450 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-2553EN" H 5200 2550 50  0001 C CNN
	1    5600 2000
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW1
U 1 1 5FAA4E64
P 1100 3150
F 0 "SW1" H 1100 3385 50  0000 C CNN
F 1 "SW_SPST" H 1100 3294 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_B3S-1000" H 1100 3150 50  0001 C CNN
F 3 "~" H 1100 3150 50  0001 C CNN
	1    1100 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	900  2600 1600 2600
$Comp
L Switch:SW_SPST SW2
U 1 1 5FAB55EF
P 1800 3150
F 0 "SW2" H 1800 3385 50  0000 C CNN
F 1 "SW_SPST" H 1800 3294 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_B3S-1000" H 1800 3150 50  0001 C CNN
F 3 "~" H 1800 3150 50  0001 C CNN
	1    1800 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 2600 2100 2600
$EndSCHEMATC
