/*  WiFi softAP Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <string.h>
#include "esp_interface.h"
#include "esp_wifi_types.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "lwip/err.h"
#include "lwip/sys.h"

#define DEFAULT_SCAN_LIST_SIZE CONFIG_EXAMPLE_SCAN_LIST_SIZE

static const char *TAG = "wifi";

static size_t get_auth_mode(int authmode, char ** res)
{
	char* auth_mode_msg = NULL;

	switch (authmode)
	{
	case WIFI_AUTH_OPEN:
		auth_mode_msg = "Authmode \tWIFI_AUTH_OPEN\0";
		break;
	case WIFI_AUTH_WEP:
		auth_mode_msg = "Authmode \tWIFI_AUTH_WEP\0";
		break;
	case WIFI_AUTH_WPA_PSK:
		auth_mode_msg = "Authmode \tWIFI_AUTH_WPA_PSK\0";
		break;
	case WIFI_AUTH_WPA2_PSK:
		auth_mode_msg = "Authmode \tWIFI_AUTH_WPA2_PSK\0";
		break;
	case WIFI_AUTH_WPA_WPA2_PSK:
		auth_mode_msg = "Authmode \tWIFI_AUTH_WPA_WPA2_PSK\0";
		break;
	case WIFI_AUTH_WPA2_ENTERPRISE:
		auth_mode_msg = "Authmode \tWIFI_AUTH_WPA2_ENTERPRISE\0";
		break;
	case WIFI_AUTH_WPA3_PSK:
		auth_mode_msg = "Authmode \tWIFI_AUTH_WPA3_PSK\0";
		break;
	case WIFI_AUTH_WPA2_WPA3_PSK:
		auth_mode_msg = "Authmode \tWIFI_AUTH_WPA2_WPA3_PSK\0";
		break;
	default:
		auth_mode_msg = "Authmode \tWIFI_AUTH_UNKNOWN\0";
		break;
	}
	
	size_t res_len = strlen(auth_mode_msg);

	if (res != NULL) {
		(*res) = malloc(sizeof(char) * (res_len + 1));
		memcpy((*res), auth_mode_msg, sizeof(char) * (res_len + 1));
	}

	return res_len;
}

static void print_cipher_type(int pairwise_cipher, int group_cipher)
{
	switch (pairwise_cipher)
	{
	case WIFI_CIPHER_TYPE_NONE:
		ESP_LOGI(TAG, "Pairwise Cipher \tWIFI_CIPHER_TYPE_NONE");
		break;
	case WIFI_CIPHER_TYPE_WEP40:
		ESP_LOGI(TAG, "Pairwise Cipher \tWIFI_CIPHER_TYPE_WEP40");
		break;
	case WIFI_CIPHER_TYPE_WEP104:
		ESP_LOGI(TAG, "Pairwise Cipher \tWIFI_CIPHER_TYPE_WEP104");
		break;
	case WIFI_CIPHER_TYPE_TKIP:
		ESP_LOGI(TAG, "Pairwise Cipher \tWIFI_CIPHER_TYPE_TKIP");
		break;
	case WIFI_CIPHER_TYPE_CCMP:
		ESP_LOGI(TAG, "Pairwise Cipher \tWIFI_CIPHER_TYPE_CCMP");
		break;
	case WIFI_CIPHER_TYPE_TKIP_CCMP:
		ESP_LOGI(TAG, "Pairwise Cipher \tWIFI_CIPHER_TYPE_TKIP_CCMP");
		break;
	default:
		ESP_LOGI(TAG, "Pairwise Cipher \tWIFI_CIPHER_TYPE_UNKNOWN");
		break;
	}

	switch (group_cipher)
	{
	case WIFI_CIPHER_TYPE_NONE:
		ESP_LOGI(TAG, "Group Cipher \tWIFI_CIPHER_TYPE_NONE");
		break;
	case WIFI_CIPHER_TYPE_WEP40:
		ESP_LOGI(TAG, "Group Cipher \tWIFI_CIPHER_TYPE_WEP40");
		break;
	case WIFI_CIPHER_TYPE_WEP104:
		ESP_LOGI(TAG, "Group Cipher \tWIFI_CIPHER_TYPE_WEP104");
		break;
	case WIFI_CIPHER_TYPE_TKIP:
		ESP_LOGI(TAG, "Group Cipher \tWIFI_CIPHER_TYPE_TKIP");
		break;
	case WIFI_CIPHER_TYPE_CCMP:
		ESP_LOGI(TAG, "Group Cipher \tWIFI_CIPHER_TYPE_CCMP");
		break;
	case WIFI_CIPHER_TYPE_TKIP_CCMP:
		ESP_LOGI(TAG, "Group Cipher \tWIFI_CIPHER_TYPE_TKIP_CCMP");
		break;
	default:
		ESP_LOGI(TAG, "Group Cipher \tWIFI_CIPHER_TYPE_UNKNOWN");
		break;
	}
}

/* Initialize Wi-Fi as sta and set scan method */
size_t wifi_scan(char ** res)
{
	uint16_t number = DEFAULT_SCAN_LIST_SIZE;
	wifi_ap_record_t ap_info[DEFAULT_SCAN_LIST_SIZE];
	uint16_t ap_count = 0;
	memset(ap_info, 0, sizeof(ap_info));

	ESP_ERROR_CHECK(esp_wifi_scan_start(NULL, true));
	ESP_ERROR_CHECK(esp_wifi_scan_get_ap_records(&number, ap_info));
	ESP_ERROR_CHECK(esp_wifi_scan_get_ap_num(&ap_count));
	
	const char* ap_count_msg = "Total APs scanned = ";
	ESP_LOGI(TAG, "%s%u\n", ap_count_msg, ap_count);

	for (int i = 0; (i < DEFAULT_SCAN_LIST_SIZE) && (i < ap_count); i++)
	{
		ESP_LOGI(TAG, "SSID \t\t%s", ap_info[i].ssid);
		ESP_LOGI(TAG, "RSSI \t\t%d", ap_info[i].rssi);
		
		char* get_auth_mode_res = NULL;
		size_t get_auth_mode_res_len = get_auth_mode(ap_info[i].authmode, &get_auth_mode_res);

		if (get_auth_mode_res_len > 0) {
			ESP_LOGI(TAG, "%s", get_auth_mode_res);
		}

		if (ap_info[i].authmode != WIFI_AUTH_WEP)
		{
			print_cipher_type(ap_info[i].pairwise_cipher, ap_info[i].group_cipher);
		}
		ESP_LOGI(TAG, "Channel \t\t%d\n", ap_info[i].primary);

		if (get_auth_mode_res_len > 0) {
			free(get_auth_mode_res);
		}
	}

	return 0;
}

static void wifi_event_handler(void *arg, esp_event_base_t event_base,
							   int32_t event_id, void *event_data)
{
	if (event_id == WIFI_EVENT_AP_STACONNECTED)
	{
		wifi_event_ap_staconnected_t *event = (wifi_event_ap_staconnected_t *)event_data;
		ESP_LOGI(TAG, "station " MACSTR " join, AID=%d",
				 MAC2STR(event->mac), event->aid);
	}
	else if (event_id == WIFI_EVENT_AP_STADISCONNECTED)
	{
		wifi_event_ap_stadisconnected_t *event = (wifi_event_ap_stadisconnected_t *)event_data;
		ESP_LOGI(TAG, "station " MACSTR " leave, AID=%d",
				 MAC2STR(event->mac), event->aid);
	}
}

void init_wifi()
{
	esp_netif_t *ap_netif = esp_netif_create_default_wifi_ap();
	assert(ap_netif);

	esp_netif_t *sta_netif = esp_netif_create_default_wifi_sta();
	assert(sta_netif);

	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	ESP_ERROR_CHECK(esp_wifi_init(&cfg));

	ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
														ESP_EVENT_ANY_ID,
														&wifi_event_handler,
														NULL,
														NULL));

	wifi_config_t wifi_config_ap = {
		.ap = {
			.ssid = CONFIG_ESP_WIFI_SSID_AP,
			.ssid_len = strlen(CONFIG_ESP_WIFI_SSID_AP),
			.channel = CONFIG_ESP_WIFI_CHANNEL_AP,
			.password = CONFIG_ESP_WIFI_PASS_AP,
			.max_connection = CONFIG_ESP_MAX_STA_CONN_AP,
			.authmode = WIFI_AUTH_WPA_WPA2_PSK},
	};
	if (strlen(CONFIG_ESP_WIFI_PASS_AP) == 0)
	{
		wifi_config_ap.ap.authmode = WIFI_AUTH_OPEN;
	}

	wifi_config_t wifi_config_sta = {
		.sta = {
			.ssid = CONFIG_ESP_WIFI_SSID,
			.channel = CONFIG_ESP_WIFI_CHANNEL,
			.password = CONFIG_ESP_WIFI_PASS},
	};

	ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_APSTA));

	ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &wifi_config_ap));
	ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config_sta));

	ESP_ERROR_CHECK(esp_wifi_start());

	ESP_LOGI(TAG, "wifi_init_softap finished. SSID:%s password:%s channel:%d",
			 CONFIG_ESP_WIFI_SSID_AP, CONFIG_ESP_WIFI_PASS_AP, CONFIG_ESP_WIFI_CHANNEL_AP);
	
	// Scan for other WIFI networks.
	char* wifi_scan_res = NULL;
	size_t wifi_scan_res_len = wifi_scan(&wifi_scan_res);

	if (wifi_scan_res_len > 0) {
		ESP_LOGI(TAG, "%s\n", wifi_scan_res);
		free(wifi_scan_res);
	}
	
	// Connect to WIFI.
	ESP_ERROR_CHECK(esp_wifi_connect());
}
