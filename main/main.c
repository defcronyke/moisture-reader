/* ADC1 Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/unistd.h>
#include <sys/stat.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/gpio.h"
#include "driver/adc.h"
#include "esp_err.h"
#include "esp_log.h"
#include "esp_spiffs.h"
#if CONFIG_IDF_TARGET_ESP32
#include "esp_adc_cal.h"
#endif
#include "nvs_flash.h"
#include "esp_netif.h"
#include "esp_event.h"
#include "lwip/err.h"
#include "lwip/sys.h"
#include "wifi.h"
#include "https_server.h"

static const char *TAG = "moisture-reader";

// GPIO pins for shift registers hooked up to 7-segment LED displays.
#define GPIO_SHIFT_SER GPIO_NUM_32
#define GPIO_SHIFT_RCLK GPIO_NUM_33
#define GPIO_SHIFT_SRCLK GPIO_NUM_25
#define GPIO_OUTPUT_PIN_SEL ((1ULL << GPIO_SHIFT_SER) | (1ULL << GPIO_SHIFT_RCLK) | (1ULL << GPIO_SHIFT_SRCLK))

// GPIO pins for sensor calibration buttons
#define GPIO_CAL_BUTTON_MIN GPIO_NUM_26
#define GPIO_CAL_BUTTON_MAX GPIO_NUM_27
#define GPIO_INPUT_PIN_SEL ((1ULL << GPIO_CAL_BUTTON_MIN) | (1ULL << GPIO_CAL_BUTTON_MAX))
#define ESP_INTR_FLAG_DEFAULT 0

// Moisture sensor calibration default values.
#define SENSOR_CAL_DEFAULT_MIN 2300.0
#define SENSOR_CAL_DEFAULT_MAX 1320.0

// Moisture sensor calibration variables.
int sensor_cal_min = SENSOR_CAL_DEFAULT_MIN;
int sensor_cal_max = SENSOR_CAL_DEFAULT_MAX;

// 7-segment LED display constants.
#define MAX_SEGMENTS_NUM 100
#define NUM_DIGITS 10

// 7-segment LED display bit patterns.
// First bit is decimal point, last bit is center segment.
const unsigned int digits[] = {
	0x7e, // 0
	0x0c,
	0xb6,
	0x9e,
	0xcc,
	0xda, // 5
	0xfa,
	0x0e,
	0xfe,
	0xde, // 9
};

#define DEFAULT_VREF 1100 //Use adc2_vref_to_gpio() to obtain a better estimate
#define NO_OF_SAMPLES 64  //Multisampling

#if CONFIG_IDF_TARGET_ESP32
static esp_adc_cal_characteristics_t *adc_chars;
static const adc_channel_t channel = ADC_CHANNEL_6; //GPIO34 if ADC1, GPIO14 if ADC2
static const adc_bits_width_t width = ADC_WIDTH_BIT_12;
#elif CONFIG_IDF_TARGET_ESP32S2
static const adc_channel_t channel = ADC_CHANNEL_6; // GPIO7 if ADC1, GPIO17 if ADC2
static const adc_bits_width_t width = ADC_WIDTH_BIT_13;
#endif
static const adc_atten_t atten = ADC_ATTEN_DB_0;
static const adc_unit_t unit = ADC_UNIT_1;

#if CONFIG_IDF_TARGET_ESP32
static void check_efuse(void)
{
	//Check TP is burned into eFuse
	if (esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_TP) == ESP_OK)
	{
		printf("eFuse Two Point: Supported\n");
	}
	else
	{
		printf("eFuse Two Point: NOT supported\n");
	}

	//Check Vref is burned into eFuse
	if (esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_VREF) == ESP_OK)
	{
		printf("eFuse Vref: Supported\n");
	}
	else
	{
		printf("eFuse Vref: NOT supported\n");
	}
}

static void print_char_val_type(esp_adc_cal_value_t val_type)
{
	if (val_type == ESP_ADC_CAL_VAL_EFUSE_TP)
	{
		printf("Characterized using Two Point Value\n");
	}
	else if (val_type == ESP_ADC_CAL_VAL_EFUSE_VREF)
	{
		printf("Characterized using eFuse Vref\n");
	}
	else
	{
		printf("Characterized using Default Vref\n");
	}
}
#endif

// Shift a 0 or 1 into a shift register.
void shift_in(int val)
{
	gpio_set_level(GPIO_SHIFT_RCLK, 0);
	gpio_set_level(GPIO_SHIFT_SER, val);
	gpio_set_level(GPIO_SHIFT_SRCLK, 1);
	gpio_set_level(GPIO_SHIFT_SRCLK, 0);
	gpio_set_level(GPIO_SHIFT_SER, 0);
	gpio_set_level(GPIO_SHIFT_RCLK, 1);
}

// Display a number on the 7-segment LED displays
// using shift registers.
void show_digits(int num)
{
	num = (int)(num > 99.0 ? 99.0 : num);

	for (int seg_i = MAX_SEGMENTS_NUM; seg_i > 0; seg_i /= 10)
	{
		for (int i = 0; i < 8; i++)
		{
			shift_in((digits[(num / seg_i) % NUM_DIGITS] >> i) & 0x1U);
		}
	}
}

/*  Map an 'input' value with a range of 'input_start' to 'input_end'
	into a different range of 'output_start' to 'output_end'.   */
double map(double input, double input_start, double input_end, double output_start, double output_end)
{
	double output = output_start + (output_end - output_start) / (input_end - input_start) * (input - input_start);

	if (output < output_start)
	{
		return output_start;
	}
	else if (output > output_end)
	{
		return output_end;
	}

	return output;
}

static xQueueHandle gpio_evt_queue = NULL;

static void IRAM_ATTR gpio_isr_handler(void *arg)
{
	uint32_t gpio_num = (uint32_t)arg;
	xQueueSendFromISR(gpio_evt_queue, &gpio_num, NULL);
}

int take_moisture_reading()
{
	uint32_t adc_reading = 0;
	//Multisampling
	for (int i = 0; i < NO_OF_SAMPLES; i++)
	{
		if (unit == ADC_UNIT_1)
		{
			adc_reading += adc1_get_raw((adc1_channel_t)channel);
		}
		else
		{
			int raw;
			adc2_get_raw((adc2_channel_t)channel, width, &raw);
			adc_reading += raw;
		}
	}
	adc_reading /= NO_OF_SAMPLES;
	double percent = map(adc_reading, sensor_cal_min, sensor_cal_max, 0.0, 100.0);
#if CONFIG_IDF_TARGET_ESP32
	//Convert adc_reading to voltage in mV
	uint32_t voltage = esp_adc_cal_raw_to_voltage(adc_reading, adc_chars);
	printf("Percent: %.4f%%\tRaw: %d\tVoltage: %dmV\tCal Min: %d\tCal Max: %d\n", percent, adc_reading, voltage, sensor_cal_min, sensor_cal_max);
#elif CONFIG_IDF_TARGET_ESP32S2
	printf("Percent: %.4f%%\tRaw: %d\tCal Min: %d\tCal Max: %d\n", percent, adc_reading, sensor_cal_min, sensor_cal_max);
#endif

	return adc_reading;
}

bool debounce_min = false;
bool debounce_max = false;

static void gpio_input_task(void *arg)
{
	uint32_t io_num;
	for (;;)
	{
		if (xQueueReceive(gpio_evt_queue, &io_num, portMAX_DELAY))
		{
			int val = gpio_get_level(io_num);

			// --- debounce ---
			if ((io_num == GPIO_CAL_BUTTON_MIN) && (val == 1) && debounce_min)
			{
				printf("debounce min\n");
				continue;
			}
			if ((io_num == GPIO_CAL_BUTTON_MAX) && (val == 1) && debounce_max)
			{
				printf("debounce max\n");
				continue;
			}

			if ((io_num == GPIO_CAL_BUTTON_MIN) && (val == 1))
			{
				debounce_min = true;
			}
			if ((io_num == GPIO_CAL_BUTTON_MAX) && (val == 1))
			{
				debounce_max = true;
			}

			if ((io_num == GPIO_CAL_BUTTON_MIN) && (val == 0))
			{
				debounce_min = false;
			}
			if ((io_num == GPIO_CAL_BUTTON_MAX) && (val == 0))
			{
				debounce_max = false;
			}
			// --- end debounce ---

			if (val == 1)
			{
				if (io_num == GPIO_CAL_BUTTON_MIN)
				{
					// Display minus signs on 7-segment LED displays
					// before min calibration.
					for (int i = 0; i < 16; i++)
					{
						shift_in((0x8080 >> i) & 0x1U);
					}

					printf("GPIO[%d], val: %d\tCalibrating sensor minimum value...\n", io_num, val);

					int raw = take_moisture_reading();

					ESP_LOGI(TAG, "Opening min file, and attempting to write value: %d", raw);
					FILE *f = fopen("/spiffs/moist-cal-min.txt", "w");
					if (f == NULL)
					{
						ESP_LOGE(TAG, "Failed to open min file for writing. Calibration min value not saved.");
					}
					else
					{
						fprintf(f, "%d\n", raw);
						fclose(f);
						ESP_LOGI(TAG, "File written. New calibration min value saved: %d", raw);
						sensor_cal_min = raw;
					}
				}
				else if (io_num == GPIO_CAL_BUTTON_MAX)
				{
					// Display two lines on 7-segment LED displays
					// before max calibration.
					for (int i = 0; i < 16; i++)
					{
						shift_in((0x1212 >> i) & 0x1U);
					}

					printf("GPIO[%d], val: %d\tCalibrating sensor maximum value...\n", io_num, val);

					int raw = take_moisture_reading();

					ESP_LOGI(TAG, "Opening max file, and attempting to write value: %d", raw);
					FILE *f = fopen("/spiffs/moist-cal-max.txt", "w");
					if (f == NULL)
					{
						ESP_LOGE(TAG, "Failed to open min file for writing. Calibration max value not saved.");
					}
					else
					{
						fprintf(f, "%d\n", raw);
						fclose(f);
						ESP_LOGI(TAG, "File written. New calibration max value saved: %d", raw);
						sensor_cal_max = raw;
					}
				}
			}
		}
	}
}

static void sample_adc1_task(void *arg)
{
	// Continuously sample ADC1
	while (1)
	{
		int raw = take_moisture_reading();

		double percent = map(raw, sensor_cal_min, sensor_cal_max, 0.0, 100.0);

		show_digits(percent);

		vTaskDelay(pdMS_TO_TICKS(1000));
	}
}

void app_main(void)
{
	// Configure the output GPIO pins for the shift
	// register.
	gpio_config_t io_conf;
	io_conf.intr_type = GPIO_INTR_DISABLE;
	io_conf.mode = GPIO_MODE_OUTPUT;
	io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL;
	io_conf.pull_down_en = 0;
	io_conf.pull_up_en = 0;
	gpio_config(&io_conf);

	// Configure the input GPIO pins for the sensor
	// calibration buttons.
	io_conf.intr_type = GPIO_INTR_ANYEDGE;
	io_conf.pin_bit_mask = GPIO_INPUT_PIN_SEL;
	io_conf.mode = GPIO_MODE_INPUT;
	io_conf.pull_down_en = 1;
	gpio_config(&io_conf);

	ESP_LOGI(TAG, "Initializing SPIFFS");

	esp_vfs_spiffs_conf_t conf = {
		.base_path = "/spiffs",
		.partition_label = NULL,
		.max_files = 5,
		.format_if_mount_failed = true};

	// Use settings defined above to initialize and mount SPIFFS filesystem.
	// Note: esp_vfs_spiffs_register is an all-in-one convenience function.
	esp_err_t ret = esp_vfs_spiffs_register(&conf);

	if (ret != ESP_OK)
	{
		if (ret == ESP_FAIL)
		{
			ESP_LOGE(TAG, "Failed to mount or format filesystem");
		}
		else if (ret == ESP_ERR_NOT_FOUND)
		{
			ESP_LOGE(TAG, "Failed to find SPIFFS partition");
		}
		else
		{
			ESP_LOGE(TAG, "Failed to initialize SPIFFS (%s)", esp_err_to_name(ret));
		}
		return;
	}

	size_t total = 0, used = 0;
	ret = esp_spiffs_info(conf.partition_label, &total, &used);
	if (ret != ESP_OK)
	{
		ESP_LOGE(TAG, "Failed to get SPIFFS partition information (%s)", esp_err_to_name(ret));
	}
	else
	{
		ESP_LOGI(TAG, "Partition size: total: %d, used: %d", total, used);
	}

	// Check if moisture sensor calibration min file exists.
	struct stat st;
	if (stat("/spiffs/moist-cal-min.txt", &st) == 0)
	{
		// Open file for reading
		ESP_LOGI(TAG, "Reading min file...");
		FILE *f = fopen("/spiffs/moist-cal-min.txt", "r");
		if (f == NULL)
		{
			ESP_LOGE(TAG, "Failed to open min file for reading. Using default value: %d", sensor_cal_min);
		}
		else
		{
			char line[64];
			fgets(line, sizeof(line), f);
			fclose(f);
			// strip newline
			char *pos = strchr(line, '\n');
			if (pos)
			{
				*pos = '\0';
			}

			// Use saved calibration value.
			sensor_cal_min = atoi(line);

			ESP_LOGI(TAG, "Found saved moisture sensor calibration minimum value. Using min: %d", sensor_cal_min);
		}
	}
	else
	{
		printf("Moisture calibration min value not saved. Using default value: %d\n", sensor_cal_min);
	}

	// Check if moisture sensor calibration max file exists.
	if (stat("/spiffs/moist-cal-max.txt", &st) == 0)
	{
		// Open file for reading
		ESP_LOGI(TAG, "Reading max file");
		FILE *f = fopen("/spiffs/moist-cal-max.txt", "r");
		if (f == NULL)
		{
			ESP_LOGE(TAG, "Failed to open max file for reading. Using default value: %d", sensor_cal_max);
		}
		else
		{
			char line[64];
			fgets(line, sizeof(line), f);
			fclose(f);
			// strip newline
			char *pos = strchr(line, '\n');
			if (pos)
			{
				*pos = '\0';
			}

			// Use saved calibration value.
			sensor_cal_max = atoi(line);

			ESP_LOGI(TAG, "Found saved moisture sensor calibration maximum value. Using max: %d", sensor_cal_max);
		}
	}
	else
	{
		printf("Moisture calibration max value not saved. Using default value: %d\n", sensor_cal_max);
	}

	// Start GPIO input reading queue and task.
	gpio_evt_queue = xQueueCreate(10, sizeof(uint32_t));
	xTaskCreate(gpio_input_task, "gpio_input_task", 4096, NULL, 10, NULL);
	gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
	gpio_isr_handler_add(GPIO_CAL_BUTTON_MIN, gpio_isr_handler, (void *)GPIO_CAL_BUTTON_MIN);
	gpio_isr_handler_add(GPIO_CAL_BUTTON_MAX, gpio_isr_handler, (void *)GPIO_CAL_BUTTON_MAX);

	// Start the output GPIO pins for the shift register
	// with LOW values.
	gpio_set_level(GPIO_SHIFT_SER, 0);
	gpio_set_level(GPIO_SHIFT_RCLK, 0);
	gpio_set_level(GPIO_SHIFT_SRCLK, 0);

#if CONFIG_IDF_TARGET_ESP32
	// Check if Two Point or Vref are burned into eFuse
	check_efuse();
#endif

	// Configure ADC
	if (unit == ADC_UNIT_1)
	{
		adc1_config_width(width);
		adc1_config_channel_atten(channel, atten);
	}
	else
	{
		adc2_config_channel_atten((adc2_channel_t)channel, atten);
	}

#if CONFIG_IDF_TARGET_ESP32
	// Characterize ADC
	adc_chars = calloc(1, sizeof(esp_adc_cal_characteristics_t));
	esp_adc_cal_value_t val_type = esp_adc_cal_characterize(unit, atten, width, DEFAULT_VREF, adc_chars);
	print_char_val_type(val_type);
#endif

	// Start sample_adc1_task.
	xTaskCreate(sample_adc1_task, "sample_adc1_task", 4096, NULL, 10, NULL);

	//Initialize NVS
	ret = nvs_flash_init();
	if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
	{
		ESP_ERROR_CHECK(nvs_flash_erase());
		ret = nvs_flash_init();
	}
	ESP_ERROR_CHECK(ret);

	ESP_ERROR_CHECK(esp_netif_init());
	ESP_ERROR_CHECK(esp_event_loop_create_default());

	// Start WIFI access point.
	init_wifi();

	// Start HTTPS server.
	init_https_server();
}
