#!/bin/sh

mkdir -p main/certs

openssl req -newkey rsa:2048 -nodes -keyout main/certs/prvtkey.pem -x509 -days 3650 -out main/certs/cacert.pem -subj "/CN=ESP32 HTTPS server example"
